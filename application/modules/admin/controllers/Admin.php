<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    // form validasi input
    public $form_conf = array(
        array('field' => 'nama', 'label' => 'Nama', 'rules' => 'required'),
        array('field' => 'username', 'label' => 'username', 'rules' => 'required'),
        array('field' => 'pass', 'label' => 'password', 'rules' => 'required'),

    );

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('model_admin');
    }

    public function index()
    {
        if (empty($this->session->userdata('id_admin'))) {
            show_404();
        }
        $this->template->title = 'Admin';
        $this->template->content->view('view_admin');
        $this->template->js->view('js_admin');
        // Publish the template
        $this->template->publish();
    }

    public function logout()
    {
        // destory session
        $this->session->sess_destroy();
        redirect('dashboard');
    }

    // tampil_admin
    public function tampil_admin()
    {
        // load library cldatatable
        $this->load->library('cldatatable');
        // tombol untuk edit & hapus di dalam tabel view
        $tombol = '<div class="text-center">
                        <button type="button" class="btn btn-success ubah-data" data-id="{{id_admin}}"  data-toggle="tooltip"> <i class="fa fa-edit"></i> </button>
                        <button type="button" class="btn btn-danger hapus-data" data-id="{{id_admin}}"  data-toggle="tooltip" title="Hapus Data"><i class="fa fa-trash"></i> </button>
                    </div>';

        // set data yang akan di tampilkan di tabel
        return $this->output->set_output($this->cldatatable->set_kolom('id_admin, nama, username')
            ->set_tabel('admin')
            ->tambah_kolom('aksi', $tombol)
            ->get_datatable());
    }

    // tambah_admin
    public function tambah_admin()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        // load and run form validasi
        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->form_conf);

        if ($this->form_validation->run($this) == false) {
            $data['pesan']  = validation_errors();
            $data['status'] = false;
            return $this->output->set_output(json_encode($data));
        }

        $id = $this->model_admin->id_admin();
        $simpan['id_admin'] = 'D' . $id;
        $simpan['nama'] = $this->input->post('nama', true);
        $simpan['username'] = $this->input->post('username', true);
        $simpan['password'] =  MD5($this->input->post('pass', true));
        $simpan['created_at']  = date('Y-m-d H:i:s');

        // load to model tambah data
        if ($this->model_admin->tambah_admin($simpan)) {
            $result['status']   = true;
            $result['pesan']    = 'Data admin berhasil ditambahkan.';
        } else {
            $error_db           = $this->model_admin->get_db_error();
            $result['status']   = false;
            $result['pesan']    = 'Data admin gagal ditambahkan.  Kesalahan kode : ' . $error_db['code'];
        }

        return $this->output->set_output(json_encode($result));
    }

    // mengambil data admin yang dipilih untuk diubah
    public function get_ubahadmin()
    {
        if (!$this->input->is_ajax_request()) {
            return;
        }

        $admin_id = $this->input->post('data_id');

        // get detail data admin
        $result = $this->model_admin->get_detail_admin($admin_id);

        if (!empty($result)) {
            $data['status'] = true;
            $data['data']   = $result;
        } else {
            $data['status'] = false;
            $data['data']   = null;
            $data['pesan']  = $this->model_admin->get_error_message();
        }
        $this->output->set_output(json_encode($data));
    }

    // ubah_admin
    public function ubah_admin()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        // load form validation
        $this->load->library('form_validation');

        // if ($this->input->post('password') != '') {
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('repassword', 'Retype Password', 'required|matches[password]');

        // run validation
        if ($this->form_validation->run($this) == false) {
            $data['pesan']  = validation_errors();
            $data['status'] = false;
            return $this->output->set_output(json_encode($data));
        }

        $id = $this->input->post('id_admin');
        $save['nama'] = $this->input->post('nama');
        $save['username'] = $this->input->post('username');
        $save['password'] = md5($this->input->post('password'));
        $save['updated_at']   = date('Y-m-d H:i:s');

        if ($this->model_admin->ubah_admin($save, $id, $this->input->post('id_admin')) == false) {
            $eror             = $this->model_admin->get_db_error();
            $result['status'] = false;
            $result['pesan']  = 'Data gagal diubah. Eror kode : ' . $eror['code'];
        } else {
            $result['status'] = true;
            $result['pesan']  = 'Data berhasil diubah.';
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    // hapus_admin
    public function hapus_admin()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        if ($this->input->post() == '') {
            $respon['status'] = false;
            $respon['pesan']  = 'Data ID tidak tersedia';
        }

        $hapus['id_admin'] = $this->input->post('data_id');

        if ($this->model_admin->hapus_admin($hapus)) {
            $data['status'] = true;
            $data['pesan']  = 'Data berhasil dihapus.';
        } else {
            $error          = $this->model_admin->get_db_error();
            $data['status'] = false;
            $data['pesan']  = 'Data produk gagal dihapus. Error kode : ' . $error['code'];
        }

        return $this->output->set_output(json_encode($data));
    }
}

/* End of file Dashboard.php */
/* Location: ./application/modules/dashboard/controllers/Dashboard.php */