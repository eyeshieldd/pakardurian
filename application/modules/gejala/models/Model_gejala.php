<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_gejala extends CI_Model
{
    //fungsi cek session
    function hapus_gejala($hapus)
    {
        return $this->db->delete('gejala', $hapus);
    }
    function tambah_gejala($simpan)
    {
      
        return $this->db->insert('gejala', $simpan);
    }
    function ubah_gejala($save, $id)
    {
        return $this->db->update('gejala', $save, array('id_gejala' => $id));
    }
    function get_detail_gejala($id_gejala)
    {
        $query = $this->db->select('id_gejala, nama_gejala')
            ->from('gejala')
            ->where('id_gejala', $id_gejala)
            ->get();

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    
    public function id_gejala()
    {
            $sql = $this->db->select('CAST(SUBSTR(id_gejala,3,4) AS INT) AS no')
            ->from('gejala')
            ->order_by('id_gejala', 'desc')
            ->limit(1)
            ->get();

       if ($sql->num_rows() > 0) {
            $result = $sql->row_array();
            $sql->free_result();
            $i     = $result['no'] + 1;
            $nomor = '00' . $i;
            return substr($nomor, -3);
        } else {
            return '001';
        }
    }
}