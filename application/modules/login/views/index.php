<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/images/durian.png') ?>">
    <title>Login Admin</title>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css"
        href="<?= base_url('assets/operator/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/operator/css/style.css') ?>" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/operator/css/colors/yellow.css') ?>" id="theme"
        rel="stylesheet"> 
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register login-sidebar"
        style="background-image:url(assets/images/background/durian-bg.jpg);">
        <div class="login-box card">
            <div class="card-body">
                <form class="form-signin" method="POST" action="<?php echo base_url('login') ?>">
                    <a href="javascript:void(0)" class="text-center db"><img
                            src="<?= base_url('assets/images/logo-icon.png') ?>" alt="Home" /><br /><img
                            src="<?= base_url('assets/images/admin1.png') ?>" alt="Home" /></a>
                    <?php if (isset($error)) {
                        echo $error;
                    }; ?>
                    <div class="form-group mt-5">
                        <div class="col-xs-12">
                            <input type="text" class="form-control" name="username" placeholder="Masukkan Username Anda"
                                autofocus>
                            <?php echo form_error('username'); ?> </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input type="password" name="password" class="form-control"
                                placeholder="Masukkan Password Anda">
                            <?php echo form_error('password'); ?> </div>
                    </div>
                    <div class="form-group text-center mt-3">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"
                                type="submit" name="btn-login" id="btn-login">Login</button>
                        </div>

                    </div>
                </form>
                <form class="form-horizontal" id="recoverform" action="index.html">
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Recover Password</h3>
                            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group text-center mt-3">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light"
                                type="submit">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?= base_url('assets/operator/plugins/jquery/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/operator/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('assets/operator/plugins/bootstrap/js/popper.min.js') ?>"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?= base_url('assets/operator/js/jquery.slimscroll.js') ?>"></script> -->
    <!--Wave Effects
    <script src="<?= base_url('assets/operator/js/waves.js') ?>"></script>
    <!--Menu sidebar -->
    <script src="<?= base_url('assets/operator/js/sidebarmenu.js') ?>"></script>
    <!--stickey kit -->
    <script src="<?= base_url('assets/operator/plugins/sticky-kit-master/dist/sticky-kit.min.js') ?>"></script>
    <!--Custom JavaScript -->
    <script src="<?= base_url('assets/operator/js/custom.min.js') ?>"></script>
    <script src="<?= base_url('assets/operator/js/circlelabs-custom.js') ?>"></script>
    <!-- <script src="<?= base_url('assets/operator/plugins/styleswitcher/jQuery.style.switcher.js') ?>"></script> -->
    <script src="<?= base_url('assets/operator/plugins/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
    <script src="<?= base_url('assets/operator/plugins/datatables.net/js/dataTables.buttons.min.js') ?>"></script>
    <script src="<?= base_url('assets/operator/js/jspdf.min.js') ?>"></script>
    <script src="<?= base_url('assets/global/toastr/toastr.min.js') ?>"></script>
    <script src="<?= base_url('assets/global/jquery-confirm-master/dist/jquery-confirm.min.js') ?>"></script>
</body>

</html>