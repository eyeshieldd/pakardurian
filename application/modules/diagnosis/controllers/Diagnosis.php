<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Diagnosis extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->library('session');
        $this->load->library('PdfGenerator');

        $this->load->model('model_diagnosis');
    }

    public function index()
    {
        // load view
        $this->template->title = 'Beranda';
        $this->template->content->view('v_diagnosisrun');

        $this->template->js->view('js_diagnosis');
        $this->template->publish();
    }

    public function hasildiagnosis()
    {
        $this->template->title = 'Hasil Diagnosis';

        $data['penyakit'] = $this->session->userdata('penyakit');
        $data['persentase'] = $this->session->userdata('persentase');
        $data['names'] = $this->session->userdata('names');

        $id = $this->session->userdata('names');
        $data['pilih'] = $this->model_diagnosis->gejala_pilih($id);


        // echo "<pre>";
        // print_r($data['pilih']);
        // exit(); 

        $this->template->content->view('v_hasildiagnosis', $data);
        $this->template->publish();
    }

    public function selesai()
    {
        $this->session->sess_destroy();
        redirect('/');
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

    public function proses()
    {
        $this->template->title = 'Diagnosis';
        $this->template->content->view('v_diagnosis');
        $this->template->js->view('js_diagnosis');
        $this->template->publish();
    }

    public function tampilgejala()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        $data['counter'] = $this->input->post('valueku');
        $data['counter'] += 1;
        $gejala['jml'] = $this->model_diagnosis->jmlgejala();
        if ($data['counter'] == $gejala['jml']) {
            $data['counter'] = 0;
        }
        $data['jalan'] = $this->model_diagnosis->get_gejala();

        return $this->output->set_output(json_encode($data));
    }

    public function pdf()
    {
        $data['penyakit'] = $this->session->userdata('penyakit');
        $data['persentase'] = $this->session->userdata('persentase');
        $data['names'] = $this->session->userdata('names');
        $id = $this->session->userdata('names');
        $data['pilih'] = $this->model_diagnosis->gejala_pilih($id);

        $html = $this->load->view('laporan_pdf', $data, true);

        $this->pdfgenerator->generate($html, 'Hasil Diagnosis');
    }

    public function prosesdiagnosis()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        $data['ss'] = $this->input->post('name');
        $data['test'] = $this->input->post('test');
        $data['jalan'] = $this->model_diagnosis->get_gejala();
        $gejala['jml'] = $this->model_diagnosis->jmlgejala();

        if ($data['ss'][0] == 1) {
            $_SESSION['names'][] = $data['test'];
            $datamu = $this->session->userdata('names');

            foreach ($datamu as $key => $value) {
                if ($key == $gejala['jml']) {
                    $this->session->unset_userdata('names');
                }
            }
        }

        $datamu = $this->session->userdata('names');
        $dataku = $this->model_diagnosis->get_gejala_all($datamu);
        
        foreach ($dataku as $key => $value) {
            $coba[$key] = $value['id_penyakit'];
        }
        $values = array_count_values($coba);
        arsort($values);
        $i = 0;
        
        foreach ($values as $key => $value) {
            $datasi[$i][$key] = $value;
            $i++;
        }

        foreach ($datasi[0] as $key => $value) {
            $dataid = $key;
            $valueid = $value;
        }

        $tren = $this->model_diagnosis->get_penyakit($dataid);
        $hasile = $this->model_diagnosis->get_persentase($dataid);
        $rumus = ($valueid / $hasile['hasil']) * 100;
        $_SESSION['penyakit'] = $tren;
        $_SESSION['persentase'] = $rumus;

        return $this->output->set_output(json_encode($data));
    }
}

/* End of file Dashboard.php */
/* Location: ./application/modules/dashboard/controllers/Dashboard.php */