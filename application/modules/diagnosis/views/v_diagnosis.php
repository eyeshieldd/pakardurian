<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="mb-0 text-dark">Diagnosis</h4>
            </div>
            <div class="card-body">
                <form id="form-tambah" onsubmit="return false">
                    <div class="block-content ">
                        <input name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" type="hidden" />
                        <div class="form-group" style="align-items: center" id="cobaa">
                            <h3>
                                <center>Apakah Tanaman Durian Anda Mengalami Gejala <span id='uname'>Ada Lubang Pada Batang</span> [<span id='iname'>G001</span>] ? </center>
                            </h3>
                        </div>

                        <form id="form-tambah" onsubmit="return false">
                            <input type="text" id="customRadio1" name="jal" value="male" class="custom-control-input">
                            <div class="row justify-content-center">
                                <div class="btn-group" data-toggle="buttons" role="group" id="hai">
                                    <label class="btn btn-outline btn-info">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio1" name="name[]" value="1" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio1"> <i class="ti-check text-active" aria-hidden="true"></i>Ya</label>
                                        </div>
                                    </label>
                                    <label class="btn btn-outline btn-info">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio2" name="name[]" value="0" class="custom-control-input">
                                            <input type="hidden" name="valueku" id="jal" value="0" class="custom-control-input">
                                            <input type="hidden" name="test" value="G001" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio2"> <i class="ti-check text-active" aria-hidden="true"></i>Tidak</label>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="btn-group" data-toggle="buttons" role="group" id="ex" style="margin-top: 12px;">
                                    <button type="submit" id="tombol-simpan" class="btn btn-md btn-success">
                                        <i class="fa fa-close"></i> Selanjutnya
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row justify-content-right" id="box"></div>
            </div>
        </div>
    </div>
</div>