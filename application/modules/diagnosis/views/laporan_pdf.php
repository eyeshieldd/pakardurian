<!DOCTYPE html>
<html>
<head>
    <title>Cetak PDF</title>
    <style type="text/css">
    #outtable{
      padding: 10px;
      border:2px solid #e3e3e3;
      width: 690px;
      border-radius: 5px;
    }
 
    .short{
      width: 50px;
    }
 
    .normal{
      width: 150px;
    }
 
    table{
      border-collapse: collapse;
      font-family: arial;
      color:#5E5B5C;
      /*color: black;*/
    }
 
    tbody th{
      text-align: left;
      padding: 10px;
    }
 
    tbody td{
      border-top: 0px solid #e3e3e3;
      padding: 10px;
    }
 
    tbody tr:nth-child(even){
      background: #F6F5FA;
    }
 
    tbody tr:hover{
      background: #EAE9F5
    }
  </style>
</head>
<body>
        <h2 class="card-title" align="center">Hasil Diagnosis Penyakit Tanaman Durian</h2>
            <div id="outtable">
            <table>
                <tbody>
                    <tr>
                        <th width="35%">Gejala Yang Dipilih</th>
                        <td>
                            <?php foreach ($pilih as $p) {
                            ?>
                                <li><?php echo $p['nama_gejala'] ?> [<?php echo $p['id_gejala'] ?>]</li>
                            <?php
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                    foreach ($penyakit as $v) {
                    ?>
                        <tr>
                            <th>Kemungkinan Penyakit Yang Terjadi</th>
                            <td><?php echo $v['nama_penyakit'] ?></td>
                        </tr>
                        <tr>
                            <th>Nilai Keakuratan Penyakit</th>
                            <td><?php echo $persentase ?>%</td>
                        </tr>
                        <tr>
                            <th>Keterangan Penyakit</th>
                            <td><?php echo $v['keterangan'] ?></td>
                        </tr>
                        <tr>
                            <th>Solusi/ Penanganan Penyakit</th>

                            <td><?php echo $v['penanganan'] ?></td>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                    <?php
                    ?>
                    <?php
                    ?>
                </tbody>
                </table>
                </div>
</body>
</html>