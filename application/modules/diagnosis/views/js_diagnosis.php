<script>
    $('#tombol-simpan').click(function() {
        var form = '#form-tambah';
        $.ajax({
            url: "<?php echo site_url('diagnosis/tampilgejala') ?>",
            type: "POST",
            data: $(form).serialize(),
            timeout: 5000,
            dataType: "JSON",
            success: function(data) {

                $('#suname', '#iname').text('');
                $('#jal').val('');
                let uname = data.jalan[data.counter].nama_gejala;
                let iname = data.jalan[data.counter].id_gejala;
                $(form + ' [name="valueku"]').val(data.counter);
                $(form + ' [name="test"]').val(iname);

                $('#iname').text(iname);
                $('#uname').text(uname);
            }

        });
    })

    $('#tombol-simpan').click(function() {
        var form = '#form-tambah';
        $.ajax({
            url: "<?php echo site_url('diagnosis/prosesdiagnosis') ?>",
            type: "POST",
            data: $(form).serialize(),
            timeout: 5000,
            dataType: "JSON",
            success: function(data) {
                if (data.test == 'G025') {
                    $("#box").append('<a href="<?= site_url('diagnosis/hasildiagnosis') ?>" class="btn btn-md btn-warning pull-left" >Hasil Diagnosis</a>');
                    $("#ex").remove();
                    $("#cobaa").remove();
                    $("#hai").remove();
                }
            }
        });
    })
</script>