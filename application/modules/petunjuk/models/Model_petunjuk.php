<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_petunjuk extends CI_Model
{
    //fungsi cek session
    function hapus_petunjuk($hapus)
    {
        return $this->db->delete('petunjuk', $hapus);
    }

    function tambah_petunjuk($simpan)
    {
        return $this->db->insert('petunjuk', $simpan);
    }

    function ubah_petunjuk($save, $id)
    {
        return $this->db->update('petunjuk', $save, array('id_petunjuk' => $id));
    }
    function get_detail_petunjuk($id_petunjuk)
    {
        $query = $this->db->select('id_petunjuk, nama_petunjuk, ket_petunjuk')
            ->from('petunjuk')
            ->where('id_petunjuk', $id_petunjuk)
            ->get();

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    function get_daftarpetunjuk()
    {
        $query = $this->db->select('nama_petunjuk, ket_petunjuk')
            ->from('petunjuk')
            // ->where('id_petunjuk', $id_petunjuk)
            ->get();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }



    public function id_petunjuk()
    {
        $sql = $this->db->select('CAST(SUBSTR(id_petunjuk,3,4) AS INT) AS no')
            ->from('petunjuk')
            ->order_by('id_petunjuk', 'desc')
            ->limit(1)
            ->get();

        if ($sql->num_rows() > 0) {
            $result = $sql->row_array();
            $sql->free_result();
            $i     = $result['no'] + 1;
            $nomor = '00' . $i;
            return substr($nomor, -3);
        } else {
            return '001';
        }
    }
}