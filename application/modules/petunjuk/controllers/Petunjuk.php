<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Petunjuk extends CI_Controller
{
    // form validasi input
    public $form_conf = array(
        array('field' => 'nama_petunjuk', 'label' => 'Nama Petunjuk', 'rules' => 'required'),
        array('field' => 'ket_petunjuk', 'label' => 'Keterangan Petunjuk', 'rules' => 'required'),
    );

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('model_petunjuk');
    }

    public function index()
    {
        // load view
        $this->template->title = 'Petunjuk';
        $this->template->content->view('view_petunjuk');
        $this->template->js->view('js_petunjuk');
        // Publish the template
        $this->template->publish();
    }

    public function tampil_petunjuk()
    {
        // load library cldatatable
        $this->load->library('cldatatable');
        // tombol untuk edit & hapus di dalam tabel view
        $tombol = '<div class="text-center">
                        <button type="button" class="btn btn-success ubah-data" data-id="{{id_petunjuk}}"  data-toggle="tooltip"> <i class="fa fa-edit"></i> </button>
                        <button type="button" class="btn btn-danger hapus-data" data-id="{{id_petunjuk}}"  data-toggle="tooltip" title="Hapus Data"><i class="fa fa-trash"></i> </button>
                    </div>';
        // set data yang akan di tampilkan di tabel
        return $this->output->set_output($this->cldatatable->set_kolom('id_petunjuk, nama_petunjuk, ket_petunjuk')
            ->set_tabel('petunjuk')
            ->tambah_kolom('aksi', $tombol)
            ->get_datatable());
    }
    
    public function user_petunjuk()
    {
        // load view
        $data['m_data'] = $this->model_petunjuk->get_daftarpetunjuk();
        $this->template->title = 'Petunjuk';
        $this->template->content->view('view_user_petunjuk', $data);

        $this->template->publish();
    }

    public function tambah_petunjuk()
    {

        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        // load and run form validasi
        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->form_conf);

        if ($this->form_validation->run($this) == false) {
            $data['pesan']  = validation_errors();
            $data['status'] = false;
            return $this->output->set_output(json_encode($data));
        }

        $id = $this->model_petunjuk->id_petunjuk();

        $simpan['id_petunjuk'] = 'T' . $id;
        $simpan['nama_petunjuk'] = $this->input->post('nama_petunjuk', true);
        $simpan['ket_petunjuk'] = $this->input->post('ket_petunjuk', true);


        // load to model tambah data
        if ($this->model_petunjuk->tambah_petunjuk($simpan)) {
            $result['status']   = true;
            $result['pesan']    = 'Data petunjuk berhasil ditambahkan.';
        } else {
            $error_db           = $this->model_petunjuk->get_db_error();
            $result['status']   = false;
            $result['pesan']    = 'Data petunjuk gagal ditambahkan.  Kesalahan kode : ' . $error_db['code'];
        }

        return $this->output->set_output(json_encode($result));
    }

    public function get_ubahpetunjuk()
    {
        if (!$this->input->is_ajax_request()) {
            return;
        }

        $id_petunjuk = $this->input->post('data_id');

        $result = $this->model_petunjuk->get_detail_petunjuk($id_petunjuk);

        if (!empty($result)) {
            $data['status'] = true;
            $data['data']   = $result;
        } else {
            $data['status'] = false;
            $data['data']   = null;
            $data['pesan']  = $this->model_petunjuk->get_error_message();
        }
        $this->output->set_output(json_encode($data));
    }

    public function ubah_petunjuk()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->form_conf);

        if ($this->form_validation->run($this) == false) {
            $data['pesan']  = validation_errors();
            $data['status'] = false;
            return $this->output->set_output(json_encode($data));
        }

        $id = $this->input->post('id_petunjuk');
        $save['nama_petunjuk'] = $this->input->post('nama_petunjuk');
        $save['ket_petunjuk'] = $this->input->post('ket_petunjuk');

        if ($this->model_petunjuk->ubah_petunjuk($save, $id, $this->input->post('id_petunjuk')) == false) {
            $eror             = $this->model_petunjuk->get_db_error();
            $result['status'] = false;
            $result['pesan']  = 'Data gagal diubah. Eror kode : ' . $eror['code'];
        } else {
            $result['status'] = true;
            $result['pesan']  = 'Data berhasil diubah.';
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function hapus_petunjuk()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        if ($this->input->post() == '') {
            $respon['status'] = false;
            $respon['pesan']  = 'Data ID tidak tersedia';
        }

        $hapus['id_petunjuk'] = $this->input->post('data_id');

        if ($this->model_petunjuk->hapus_petunjuk($hapus)) {
            $data['status'] = true;
            $data['pesan']  = 'Data berhasil dihapus.';
        } else {
            $error          = $this->model_petunjuk->get_db_error();
            $data['status'] = false;
            $data['pesan']  = 'Data produk gagal dihapus. Error kode : ' . $error['code'];
        }

        return $this->output->set_output(json_encode($data));
    }
}

/* End of file Dashboard.php */
/* Location: ./application/modules/dashboard/controllers/Dashboard.php */