<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Informasi Daftar Penyakit</h3>
                <h6 class="card-subtitle">Informasi tentang hama atau penyakit yang terdapat pada tanaman durian</h6>
                <table data-toggle="table" data-height="500" data-mobile-responsive="true" class="table-striped">
                    <thead>
                        <tr>
                            <th>Nama Penyakit</th>
                            <th>Definisi Penyakit</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($m_data as $v_penyakit) {
                        ?>
                        <tr id="tr-id-1" class="tr-class-1">
                            <td><?php echo $v_penyakit['nama_penyakit'] ?></td>
                            <td><?php echo $v_penyakit['keterangan'] ?></td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>