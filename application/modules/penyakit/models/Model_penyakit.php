<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_penyakit extends CI_Model
{
    //fungsi cek session
    function hapus_penyakit($hapus)
    {
        return $this->db->delete('penyakit', $hapus);
    }
    function tambah_penyakit($simpan)
    {
        return $this->db->insert('penyakit', $simpan);
    }
    function ubah_penyakit($save, $id)
    {
        return $this->db->update('penyakit', $save, array('id_penyakit' => $id));
    }
    function get_detail_penyakit($id_penyakit)
    {
        $query = $this->db->select('id_penyakit, nama_penyakit, keterangan, penanganan')
            ->from('penyakit')
            ->where('id_penyakit', $id_penyakit)
            ->get();

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    function get_daftarpenyakit()
    {
        $query = $this->db->select('penyakit.nama_penyakit, penyakit.keterangan')
            ->from('penyakit')
            // ->where('id_petunjuk', $id_petunjuk)
            ->get();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function id_penyakit()
    {
        $sql = $this->db->select('CAST(SUBSTR(id_penyakit,2,4) AS INT) AS no')
            ->from('penyakit')
            ->order_by('id_penyakit', 'desc')
            ->limit(1)
            ->get();

        if ($sql->num_rows() > 0) {
            $result = $sql->row_array();
            $sql->free_result();
            $i     = $result['no'] + 1;
            $nomor = '0' . $i;
            return substr($nomor, -2);
        } else {
            return '01';
        }
    }
}