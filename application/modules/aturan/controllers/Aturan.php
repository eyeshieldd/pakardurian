<?php
defined('BASEPATH') or exit('No direct script access allowed');

class aturan extends CI_Controller
{
    // form validasi input
    public $form_conf = array(
        array('field' => 'gejala', 'label' => 'Nama aturan', 'rules' => 'required'),
        array('field' => 'penyakit', 'label' => 'Nama aturan', 'rules' => 'required'),
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_aturan');
        $this->load->helper(array('url'));
    }

    public function index()
    {
        if (empty($this->session->userdata('id_admin'))) {
            show_404();
        } 
        $data['gejala'] = $this->model_aturan->get_gejala();
        $data['penyakit'] =  $this->model_aturan->get_penyakit();
        $this->template->content->view('view_aturan', $data);
        $this->template->title = 'Aturan';
        // Publish the template
        $this->template->js->view('js_aturan');
        $this->template->publish();
    }

    public function tampil_aturan()
    {
        if (empty($this->session->userdata('id_admin'))) {
            show_404();
        }
        // load library cldatatable
        $this->load->library('cldatatable');
        // tombol untuk edit & hapus di dalam tabel view
        $tombol = '<div class="text-center">
                         <button type="button" class="btn btn-success ubah-data" data-id="{{id_aturan}}"  data-toggle="tooltip"> <i class="fa fa-edit"></i> </button>
                        <button type="button" class="btn btn-danger hapus-data" data-id="{{id_aturan}}"  data-toggle="tooltip" title="Hapus Data"><i class="fa fa-trash"></i> </button>
                  </div>';
        // set data yang akan di tampilkan di tabel
        return $this->output->set_output($this->cldatatable->set_kolom('a.id_aturan,b.nama_penyakit,c.nama_gejala')
            ->set_tabel('aturan a')
            ->join_tabel('penyakit b', 'b.id_penyakit = a.id_penyakit', 'inner')
            ->join_tabel('gejala c', 'c.id_gejala = a.id_gejala', 'inner')
            ->tambah_kolom('aksi', $tombol)
            ->get_datatable());
    }

    public function tambah_aturan()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $jum = count($this->input->post('gejala'));
        $id = $this->model_aturan->id_aturan();
        for ($i = 0; $i < $jum; $i++) {
            $simpan['id_penyakit'][$i] = $this->input->post('penyakit');
        }
        for ($j = $id; $j < $jum + $id; $j++) {
            $no[$j] = '00' . $j;
            $generate[$j] = substr($no[$j], -3);
            $simpan['id_aturan'][$j] = 'A' . $generate[$j];
        }

        $simpan['id_gejala'] = $this->input->post('gejala');

        $data = array();
        $i = 0;
        foreach ($simpan as $key => $val) {
            $i = 0;
            foreach ($val as $k => $v) {
                $data[$i][$key] = $v;
                $i++;
            }
        }
        // load to model tambah data
        if ($this->model_aturan->tambah_aturan($data)) {
            $result['status']   = true;
            $result['pesan']    = 'Data aturan berhasil ditambahkan.';
        } else {
            $error_db           = $this->model_aturan->get_db_error();
            $result['status']   = false;
            $result['pesan']    = 'Data aturan gagal ditambahkan.  Kesalahan kode : ' . $error_db['code'];
        }
        return $this->output->set_output(json_encode($result));
    }

    public function get_ubahaturan()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $id_aturan = $this->input->post('data_id');

        $result = $this->model_aturan->get_detail_aturan($id_aturan);

        if (!empty($result)) {
            $data['status'] = true;
            $data['data']   = $result;
        } else {
            $data['status'] = false;
            $data['data']   = null;
            $data['pesan']  = $this->model_aturan->get_error_message();
        }
        $this->output->set_output(json_encode($data));
    }

    public function ubah_aturan()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        // $this->load->library('form_validation');
        // $this->form_validation->set_rules($this->form_conf);

        // if ($this->form_validation->run($this) == false) {
        //     $data['pesan']  = validation_errors();
        //     $data['status'] = false;
        //     return $this->output->set_output(json_encode($data));
        // }

        $id = $this->input->post('id_aturan');
        $save['id_penyakit'] = $this->input->post('penyakit');
        $save['id_gejala'] = $this->input->post('gejala');

        if ($this->model_aturan->ubah_aturan($save, $id, $this->input->post('id_aturan')) == false) {
            $eror             = $this->model_aturan->get_db_error();
            $result['status'] = false;
            $result['pesan']  = 'Data gagal diubah. Eror kode : ' . $eror['code'];
        } else {
            $result['status'] = true;
            $result['pesan']  = 'Data berhasil diubah.';
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function hapus_aturan()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        if ($this->input->post() == '') {
            $respon['status'] = false;
            $respon['pesan']  = 'Data ID tidak tersedia';
        }

        $hapus['id_aturan'] = $this->input->post('data_id');

        if ($this->model_aturan->hapus_aturan($hapus)) {
            $data['status'] = true;
            $data['pesan']  = 'Data berhasil dihapus.';
        } else {
            $error          = $this->model_aturan->get_db_error();
            $data['status'] = false;
            $data['pesan']  = 'Data produk gagal dihapus. Error kode : ' . $error['code'];
        }
        return $this->output->set_output(json_encode($data));
    }
}

/* End of file Dashboard.php */
/* Location: ./application/modules/dashboard/controllers/Dashboard.php */