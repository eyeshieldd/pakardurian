<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_aturan extends CI_Model
{
    //fungsi cek session
    function hapus_aturan($hapus)
    {
        return $this->db->delete('aturan', $hapus);
    }

    function tambah_aturan($data)
    {
        return $this->db->insert_batch('aturan', $data);
    }

    function ubah_aturan($save, $id)
    {
        return $this->db->update('aturan', $save, array('id_aturan' => $id));
    }

    function get_detail_aturan($id_aturan)
    {
        $query = $this->db->select('id_aturan, id_gejala')
            ->from('aturan ')
            ->where('id_aturan', $id_aturan)
            ->get();

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    function get_penyakit()
    {
        $query = $this->db->select('id_penyakit, nama_penyakit')
            ->from('penyakit')
            ->get();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    function get_gejala()
    {
        $query = $this->db->select('id_gejala, nama_gejala')
            ->from('gejala')
            ->get();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function id_aturan()
    {
        $sql = $this->db->select('CAST(SUBSTR(id_aturan,3,4) AS INT) AS no')
            ->from('aturan')
            ->order_by('id_aturan', 'desc')
            ->limit(1)
            ->get();

        if ($sql->num_rows() > 0) {
            $result = $sql->row_array();
            $sql->free_result();
            $i     = $result['no'] + 1;
            $nomor =  $i;

            return $nomor;
        } else {
            return '1';
        }
    }

    // public function user_is_exist($user_mail = null, $user_id = null)
    // {
    //     $this->db->select('COUNT(id_admin) as jumlah')
    //         ->from('admin')
    //         ->where('username', $user_mail);

    //     if ($user_id != null) {
    //         $this->db->where('id_admin !=', $user_id);
    //     }

    //     $sql = $this->db->get();
    //     if ($sql->num_rows() > 0) {
    //         $result = $sql->row_array();
    //         $sql->free_result();
    //         return ($result['jumlah'] > 0);
    //     } else {
    //         return false;
    //     }
    // }

  
}
