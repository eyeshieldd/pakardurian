<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <button class="btn float-right hidden-sm-down btn-success" data-toggle="modal" data-target="#modal-tambah"><i class="mdi mdi-plus-circle"></i>Tambah</button>
                <h4 class="card-title">Data aturan</h4>
                <div class="table-responsive m-t-40">
                    <table id="taturan" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama Penyakit</th>
                                <th>Nama Gejala</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL TAMBAH DATA ADMIN -->
<div class="modal fade bs-example-modal-lg" id="modal-tambah" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="block-title">Tambah aturan</h3>
                <div class="block-options">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <form id="form-tambah" onsubmit="return false">
                    <div class="block-content">
                        <input name="<?php echo $this->security->get_csrf_token_name(); ?>"
                            value="<?php echo $this->security->get_csrf_hash(); ?>" type="hidden" />
                        <div class="form-group">
                        <input name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" type="hidden" />

                        <div class="form-group">
                            <label>Masukan Penyakit</label>
                            <select class="custom-select col-12" id="tambah" name="penyakit">
                                <option selected>-Pilih-</option>
                                <?php
                                if (isset($penyakit) && !empty($penyakit)) {
                                    foreach ($penyakit as $vpenyakit) {
                                        echo '<option value="' . $vpenyakit['id_penyakit'] . '">' . $vpenyakit['nama_penyakit'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                        <div class="form-group row pt-2">
                            <div class="col-sm-4">
                                <label>Gejala</label>
                                <?php
                                if (isset($gejala) && !empty($gejala)) {
                                    foreach ($gejala as $vgejala) {
                                        echo '<div class="form-check" id ="ajax_content">';
                                        echo '<input type="checkbox" name="gejala[]" value="' . $vgejala['id_gejala'] . '" class="form-check-input" id="exampleCheck1">';
                                        echo '<label class="form-check-label" for="exampleCheck1">' . $vgejala['nama_gejala'] . '</label>';
                                        echo '</div>';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-md btn-success pull-right" id="tombol-submit">
                            <i class="fa fa-save"></i> Simpan
                        </button>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-dark pull-right">
                            <i class="fa fa-close"></i> Batal
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ubah" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="block-title">Ubah Aturan</h3>
                <div class="block-options">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <form id="form-ubah" onsubmit="return false">
                    <div class="block-content">
                        <input name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" type="hidden" />
                        <input type="hidden" name="id_aturan" />

                        <div class="form-group">
                            <label>Masukkan Penyakit</label>
                            <select class="custom-select col-12" id="ubah" name="penyakit">
                                <option name="penyakit"></option>
                                <?php
                                if (isset($penyakit) && !empty($penyakit)) {
                                    foreach ($penyakit as $vpenyakit) {
                                        echo '<option value="' . $vpenyakit['id_penyakit'] . '">' . $vpenyakit['nama_penyakit'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Masukkan Gejala</label>
                            <select class="custom-select col-12" id="ubah" name="gejala">
                                <option name="gejala"></option>
                                <?php
                                if (isset($gejala) && !empty($gejala)) {
                                    foreach ($gejala as $vgejala) {
                                        echo '<option value="' . $vgejala['id_gejala'] . '">' . $vgejala['nama_gejala'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-md btn-success pull-right" id="tombol-submit">
                                <i class="fa fa-save"></i> Simpan
                            </button>
                            <button type="button" data-dismiss="modal" class="btn btn-md btn-dark pull-right">
                                <i class="fa fa-close"></i> Cancel
                            </button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>